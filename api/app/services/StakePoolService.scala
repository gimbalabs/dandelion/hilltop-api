package services

import akka.stream.Materializer
import akka.stream.scaladsl.Source
import dao.{EpochDao, StakePoolData, StakePoolsDao}
import io.gimbalabs.hilltop.api.v0.models.json._
import io.gimbalabs.hilltop.api.v0.models.{Stakepool, StakepoolMetadata}
import play.api.Logging
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

@Singleton
class StakePoolService @Inject()(epochDao: EpochDao,
                                 stakePoolsDao: StakePoolsDao)(implicit materializer: Materializer, executionContext: ExecutionContext) extends Logging {

  private var activeStakepools = List.empty[Stakepool]

  private var retiredStakepools = List.empty[Stakepool]

  Source
    .tick(0.minutes, 60.minutes, ())
    .map { _ =>
      logger.info("Refreshing active and retiring stake pools ")
      val currentEpochNumber = epochDao.currentEpoch.no
      val activeStakepools = stakePoolsDao.activeStakePools(currentEpochNumber).map(toStakepool)
      val retiredStakepools = stakePoolsDao.retiredStakepool().map(toStakepool)
      (activeStakepools, retiredStakepools)
    }
    .runForeach {
      case (currentActiveStakePools, currentRetiredStakePools) =>
        activeStakepools = currentActiveStakePools
        retiredStakepools = currentRetiredStakePools
    }

  private def toStakepool(stakepoolData: StakePoolData) = {
    val poolId = new String(Hex.encodeHex(stakepoolData.poolId))
    Stakepool(
      poolId,
      stakepoolData.poolIdBech32,
      stakepoolData.poolMetadataJson.map(_.as[StakepoolMetadata]),
      stakepoolData.fixedCost,
      stakepoolData.margin,
      stakepoolData.pledge,
      stakepoolData.activeStake,
      stakepoolData.saturation,
      stakepoolData.retiringEpoch
    )
  }

  def getActiveStakepools: Seq[Stakepool] = activeStakepools

  def getRetiredStakepools: Seq[Stakepool] = retiredStakepools

}
