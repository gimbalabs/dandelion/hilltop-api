package services

import dao.LedgerDao
import play.api.Logging

import javax.inject.{Inject, Singleton}

@Singleton
class BalanceService @Inject()(ledgerDao: LedgerDao) extends Logging {


  def getBalance(stakingAddress: String) = {
    val utxoBalance = ledgerDao.getWalletBalance(stakingAddress).getOrElse(0L)
    val rewards = ledgerDao.getRewards(stakingAddress).getOrElse(0L)
    val withdrawals = ledgerDao.getWithdrawals(stakingAddress).getOrElse(0L)
    utxoBalance + rewards - withdrawals
  }

}
