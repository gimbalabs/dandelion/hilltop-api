package db

import org.joda.time.DateTime

package object model {

  case class Epoch(outSum: Long, fees: Long, txCount: Int, blkCount: Int, no: Int, startTime: DateTime, endTime: DateTime)

  case class EpochParam(id: Long, epoch_no: Int, nonce: Array[Byte])

}
