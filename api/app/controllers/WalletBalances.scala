package controllers

import io.gimbalabs.hilltop.api.v0.models.WalletBalance
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import services.BalanceService

import javax.inject.{Inject, Singleton}

@Singleton
class WalletBalances @Inject()(balanceService: BalanceService) extends InjectedController {

  def getByStakingAddress(stakingAddress: String) = Action {
    Ok(Json.toJson(WalletBalance(balanceService.getBalance(stakingAddress))))
  }

}