package controllers

import dao.{PoolRetireDao, PoolUpdateDao}
import io.gimbalabs.hilltop.api.v0.models.json._
import io.gimbalabs.hilltop.api.v0.models.{GenericError, PoolRetiring, PoolUpdate}
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

@Singleton
class PoolUpdates @Inject()(poolUpdateDao: PoolUpdateDao, poolRetireDao: PoolRetireDao) extends InjectedController {

  private def toModel(poolUpdate: dao.PoolUpdate): PoolUpdate = PoolUpdate(
    poolUpdate.epochNo,
    poolUpdate.blockNumber,
    new String(Hex.encodeHex(poolUpdate.txHash)),
    poolUpdate.tickerName,
    new String(Hex.encodeHex(poolUpdate.hashRaw)),
    poolUpdate.certIndex,
    new String(Hex.encodeHex(poolUpdate.vrfKeyHash)),
    poolUpdate.pledge,
    new String(Hex.encodeHex(poolUpdate.rewardAddr)),
    poolUpdate.activeEpochNo,
    poolUpdate.margin,
    poolUpdate.fixedCost
  )

  def get(blockNumberFrom: Option[Long], limit: Option[Long]) = Action {
    poolUpdateDao
      .findAll(blockNumberFrom, limit)
      .map(toModel) match {
      case Nil => NotFound(Json.toJson(GenericError("No pool updates found")))
      case poolUpdates => Ok(Json.toJson(poolUpdates))
    }

  }

  def getByPoolId(poolId: String, blockNumberFrom: Option[Long], limit: Option[Long]) = Action {
    poolUpdateDao
      .findAllByPoolBech32Id(poolId, blockNumberFrom, limit)
      .map(toModel) match {
      case Nil => NotFound(Json.toJson(GenericError("No pool updates found")))
      case poolUpdates => Ok(Json.toJson(poolUpdates))
    }
  }

  def getRetiring(epochNumberFrom: Option[Long], limit: Option[Long]) = Action {
    poolRetireDao
      .findAll(epochNumberFrom, limit) match {
      case Nil => NotFound(Json.toJson(GenericError("No retiring pools found")))
      case dbRetiringPools =>
        val reiringPools = dbRetiringPools.map(poolRetire => PoolRetiring(
          poolRetire.tickerName,
          new String(Hex.encodeHex(poolRetire.hashRaw)),
          poolRetire.certIndex,
          poolRetire.epochNumber,
          poolRetire.blockNumber,
          poolRetire.retiringEpoch,
          new String(Hex.encodeHex(poolRetire.txHash))
        ))
        Ok(Json.toJson(reiringPools))
    }

  }

}