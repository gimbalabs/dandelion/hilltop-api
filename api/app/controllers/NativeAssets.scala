package controllers

import dao.NativeAssetsDao
import io.gimbalabs.hilltop.api.v0.models._
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.libs.json.{JsNull, JsString, Json}
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import java.util.concurrent.Executors
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@Singleton
class NativeAssets @Inject()(nativeAssetsDao: NativeAssetsDao) extends InjectedController {

  private val executors = Executors.newCachedThreadPool()

  private implicit val ec = ExecutionContext.fromExecutor(executors)

  private val NFTMetadataKey = 721L

  def getByStakeAddress(stakeAddress: String) = Action.async {
    Future {
      val assets = nativeAssetsDao
        .findByStakeAddress(stakeAddress)
        .map(asset => NativeAsset(
          asset.txId,
          asset.policyId,
          asset.tokenName,
          asset.metadataJson,
          asset.blockNo,
          asset.epochSlotNo,
          asset.epochNo,
          asset.invalidBefore,
          asset.invalidHereafter,
          asset.quantity))
      Ok(Json.toJson(assets))
    }
  }


  def getIds(blockNumberFrom: Option[Long], limit: Option[Long]) = Action.async {
    Future {
      val mintingTransactions = nativeAssetsDao
        .findAllMintingTransactions(blockNumberFrom, limit)
      val nativeAssetIds = mintingTransactions.map(mintingTx => {
        NativeAssetId(new String(Hex.encodeHex(mintingTx.policy)), new String(mintingTx.name), mintingTx.blockNo, mintingTx.blockTime, mintingTx.fingerprint)
      })
      Ok(Json.toJson(nativeAssetIds))
    }
  }

  def getAssetNamesByPolicyId(policyId: String) = Action.async {
    Future {
      val assetNames = nativeAssetsDao
        .findAssetNameForPolicy(policyId)
        .map(nameBytes => new String(nameBytes))
      Ok(Json.toJson(assetNames))
    }
  }

  def postNativeAssetsDetails = Action.async(parse.json[NativeAssetDetailsRequest]) { implicit req =>
    Future {
      val request = req.body

      val assetNames = request
        .assetNames match {
        case Some(assetNames) => assetNames
        case None => nativeAssetsDao
          .findAllMintingTransactions(request.policyId)
          .map(mintingTx => new String(mintingTx.name))
      }

      val nativeAssetDetails = nativeAssetsDao
        .findAllMintingTransactionsDetails(request.policyId, assetNames)
        .groupBy(tx => new String(tx.name))
        .flatMap {
          case (assetName, tx) =>
            val mintedAt = tx.minBy(_.mintedAt).mintedAt
            val lastMintedAt = tx.maxBy(_.mintedAt).mintedAt
            val firstMintingTxBlock = tx.minBy(_.mintedAtBlock).mintedAtBlock
            val lastMintingTxBlock = tx.maxBy(_.mintedAtBlock).mintedAtBlock
            val circulatingSupply = tx.map(_.mintedQuantity).sum
            for {
              mintingTransaction <- tx.filter(assetDetails => assetDetails.mintedQuantity > 0L && assetDetails.metadataKey.contains(NFTMetadataKey)).maxByOption(_.mintedAtBlock)
              metadataJson <- mintingTransaction.metadataJson
              txHash = new String(Hex.encodeHex(mintingTransaction.txHash))
            } yield {
              NativeAssetDetails(request.policyId, assetName, metadataJson, mintedAt, lastMintedAt, firstMintingTxBlock, lastMintingTxBlock, txHash, circulatingSupply, mintingTransaction.fingerprint)
            }
        }
        .toList
      nativeAssetDetails match {
        case Nil => NotFound
        case details => Ok(Json.toJson(details))
      }
    }
  }

  def getNfts(blockNumberFrom: Long, blockNumberTo: Long) = Action.async {
    if (blockNumberTo < blockNumberFrom) {
      Future.successful(BadRequest(Json.toJson(GenericError("Invalid range"))))
    } else {
      Future {
        nativeAssetsDao
          .findNftMintingTransactionsDetailsAtBlock(blockNumberFrom, blockNumberTo)
          .flatMap { nativeAssetDetails =>
            val policyId = new String(Hex.encodeHex(nativeAssetDetails.policy))
            val assetNameRaw = new String(Hex.encodeHex(nativeAssetDetails.name))
            val assetNameOpt = Try(new String(nativeAssetDetails.name)).toOption
            val txHash = new String(Hex.encodeHex(nativeAssetDetails.txHash))
            nativeAssetDetails
              .metadataJson
              .map { json =>

                val leanerMetadata = for {
                  assetName <- assetNameOpt
                  nftMetadata <- (json \ policyId \ assetName).toOption
                } yield {
                  Json.obj(
                    "version" -> (json \ "version").toOption,
                    policyId -> Json.obj(assetName -> nftMetadata)
                  )
                }

                NftDetails(policyId,
                  assetNameRaw,
                  assetNameOpt,
                  leanerMetadata.getOrElse(json),
                  nativeAssetDetails.mintedAt,
                  nativeAssetDetails.mintedAtBlock,
                  txHash,
                  nativeAssetDetails.mintedQuantity,
                  nativeAssetDetails.fingerprint
                )
              }
          }
      }.map(nftDetails => Ok(Json.toJson(nftDetails)))
    }

  }

}