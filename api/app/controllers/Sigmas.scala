package controllers

import dao.{ActiveStakeDao, EpochDao, EpochParamDao, PoolHash, PoolHashDao}
import io.gimbalabs.hilltop.api.v0.models.Sigma
import io.gimbalabs.hilltop.api.v0.models.json._
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

@Singleton
class Sigmas @Inject()(epochDao: EpochDao,
                       epochParamDao: EpochParamDao,
                       poolHashDao: PoolHashDao,
                       activeStakeDao: ActiveStakeDao) extends InjectedController {

  def getByPoolId(poolId: String, epochNumber: Int) = Action {

    if (epochNumber > epochDao.currentEpoch.no) {
      BadRequest("Sigmas from future epochs cannot be computed")
    } else {

      val f: String => Option[PoolHash] = if (poolId.startsWith("pool1")) {
        poolHashDao.findByPoolId
      } else {
        poolHashDao.findByHashRaw
      }

      (for {
        poolId <- f(poolId).map(_.id)
        epochParam <- epochParamDao.findByEpochNumber(epochNumber)
      } yield {
        val activeStakeInEpoch = activeStakeDao.activeStakeByEpochNumber(epochNumber)
        val poolActiveStakeInEpoch = activeStakeDao.activeStakeByEpochNumber(epochNumber, poolId)
        val sigma = poolActiveStakeInEpoch.toDouble / activeStakeInEpoch.toDouble
        val nonce = new String(Hex.encodeHex(epochParam.nonce))
        Ok(Json.toJson(Sigma(sigma, epochNumber, activeStakeInEpoch, poolActiveStakeInEpoch, nonce, 0.0)))
      }).getOrElse {
        NotFound(s"Pool could not be found (Bech32: $poolId)")
      }

    }

  }

}