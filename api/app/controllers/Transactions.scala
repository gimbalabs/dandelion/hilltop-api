package controllers

import dao._
import io.gimbalabs.hilltop.api.v0.models.json._
import io.gimbalabs.hilltop.api.v0.models.{Transaction, TransactionHistoryRequest}
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

@Singleton
class Transactions @Inject()(ledgerDao: LedgerDao) extends InjectedController {

  def postHistory = Action(parse.json[TransactionHistoryRequest]) { implicit req =>
    val txs = ledgerDao
      .transactionHistory(req.body.addresses)
      .map(transaction => Transaction(new String(Hex.encodeHex(transaction.hash)), transaction.blockNo, transaction.time))
    Ok(Json.toJson(txs))
  }

}