package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL, SqlParser}
import com.google.inject.{Inject, Singleton}
import org.joda.time.DateTime
import play.api.db.Database

case class WalletTransaction(id: Long, hash: Array[Byte], blockNo: Long, blockHash: Array[Byte], time: DateTime, blockIndex: Long)

@Singleton
class LedgerDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[WalletTransaction](ColumnNaming.SnakeCase)

  def transactionHistory(addresses: Seq[String]) = database.withConnection { implicit c =>
    SQL(
      """
        |  SELECT
        |    tx.id, tx.hash, block.block_no, block.hash as block_hash, block.time, tx.block_index
        |    FROM block
        |    INNER JOIN tx ON block.id = tx.block_id
        |    INNER JOIN tx_out ON tx.id = tx_out.tx_id
        |    WHERE tx_out.address IN ({addresses})
        |  UNION
        |    SELECT DISTINCT
        |      tx.id, tx.hash, block.block_no, block.hash as blockHash, block.time, tx.block_index
        |      FROM block
        |      INNER JOIN tx ON block.id = tx.block_id
        |      INNER JOIN tx_in ON tx.id = tx_in.tx_in_id
        |      INNER JOIN tx_out ON (tx_in.tx_out_id = tx_out.tx_id) AND (tx_in.tx_out_index = tx_out.index)
        |      WHERE tx_out.address IN ({addresses})
        |      ORDER BY time DESC
        |""".stripMargin)
      .on("addresses" -> addresses)
      .as(parser.*)
  }

  def getWalletBalance(stakingAddress: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT sum(ev.value) FROM utxo_view ev
        | INNER JOIN stake_address sa
        |   ON ev.stake_address_id = sa.id
        | WHERE sa.view = {stakingAddress}
        |""".stripMargin)
      .on("stakingAddress" -> stakingAddress)
      .as(SqlParser.scalar[Long].singleOpt)
  }

  def getRewards(stakingAddress: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT sum(r.amount) FROM reward r
        | INNER JOIN stake_address sa
        |   ON r.addr_id = sa.id
        | WHERE sa.view = {stakingAddress}
        |""".stripMargin)
      .on("stakingAddress" -> stakingAddress)
      .as(SqlParser.scalar[Long].singleOpt)
  }

  def getWithdrawals(stakingAddress: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT sum(w.amount) FROM withdrawal w
        | INNER JOIN stake_address sa
        |   ON w.addr_id = sa.id
        | WHERE sa.view = {stakingAddress}
        |""".stripMargin)
      .on("stakingAddress" -> stakingAddress)
      .as(SqlParser.scalar[Long].singleOpt)
  }

  def getSafePaymentAddress(stakingAddress: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT tx_out.address
        | FROM tx_out
        | INNER JOIN tx_in
        |   ON tx_out.tx_id = tx_in.tx_out_id
        | INNER JOIN tx
        |   ON tx.id = tx_in.tx_in_id AND tx_in.tx_out_index = tx_out.index
        | INNER JOIN stake_registration sr
        |   ON tx.id = sr.tx_id
        | INNER JOIN stake_address sa
        |   ON sr.addr_id = sa.id
        | WHERE sa.view = {stakingAddress};
        |""".stripMargin)
      .on("stakingAddress" -> stakingAddress)
      .as(SqlParser.scalar[String].*)
      .headOption
  }

}
