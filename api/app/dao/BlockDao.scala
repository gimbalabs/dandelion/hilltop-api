package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL}
import com.google.inject.{Inject, Singleton}
import org.joda.time.{DateTime, LocalDate}
import play.api.db.Database

import java.sql.Timestamp

case class Block(number: Option[Long], epochNumber: Option[Long], slotNumber: Option[Long], epochSlotNumber: Option[Long], size: Long, transactionCount: Long, time: DateTime)

@Singleton
class BlockDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[Block](ColumnNaming.SnakeCase)

  def findByBlockNumber(blockNumber: Long) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT hash, epoch_no as epoch_number, slot_no as slot_number, epoch_slot_no as epoch_slot_number,
        | block_no as number, size, time at time zone 'UTC' as time, tx_count as transaction_count
        | FROM block
        | WHERE block_no = {blockNumber}
        |""".stripMargin)
      .on("blockNumber" -> blockNumber)
      .as(parser.singleOpt)
  }

  def findBlockRangeInDay(date: LocalDate) = database.withConnection { implicit c =>
    val first = SQL(
      """
        | SELECT hash, epoch_no as epoch_number, slot_no as slot_number, epoch_slot_no as epoch_slot_number,
        | block_no as number, size, time at time zone 'UTC' as time, tx_count as transaction_count
        | FROM block
        | WHERE time >= {date}
        | ORDER BY time asc
        | LIMIT 1
        |""".stripMargin)
      .on("date" -> new Timestamp(date.toDate.getTime))
      .as(parser.singleOpt)

    val last = SQL(
      """
        | SELECT hash, epoch_no as epoch_number, slot_no as slot_number, epoch_slot_no as epoch_slot_number,
        | block_no as number, size, time at time zone 'UTC' as time, tx_count as transaction_count
        | FROM block
        | WHERE time >= {date}
        | ORDER BY time asc
        | LIMIT 1
        |""".stripMargin)
      .on("date" -> new Timestamp(date.plusDays(1).toDate.getTime))
      .as(parser.singleOpt)
    (first, last)
  }

}
