package dao

import anorm.Macro.ColumnNaming
import anorm._
import play.api.db.Database

import javax.inject.{Inject, Singleton}

case class PoolRetire(tickerName: Option[String],
                      hashRaw: Array[Byte],
                      epochNumber: Long,
                      blockNumber: Long,
                      certIndex: Int,
                      txHash: Array[Byte],
                      retiringEpoch: Long)

@Singleton
class PoolRetireDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[PoolRetire](ColumnNaming.SnakeCase)

  def findAll(minBlockNumber: Option[Long] = None, limit: Option[Long] = Some(1000L)) = database.withConnection { implicit c =>
    SQL(
      """
        | WITH
        |  stakepool_ticker_name as ( SELECT DISTINCT ON (pool_id) pool_id, ticker_name FROM pool_offline_data ORDER BY pool_id, id DESC )
        | SELECT stn.ticker_name, ph.hash_raw, b.epoch_no as epoch_number, b.block_no AS block_number, pr.cert_index, tx.hash AS tx_hash, pr.retiring_epoch
        | FROM pool_retire pr
        | INNER JOIN pool_hash ph
        |   ON pr.hash_id = ph.id
        | INNER JOIN tx
        |   ON pr.announced_tx_id = tx.id
        | INNER JOIN block b
        |   ON tx.block_id = b.id
        | LEFT OUTER JOIN stakepool_ticker_name stn
        |   ON pr.hash_id = stn.pool_id
        | WHERE b.block_no >= {minBlockNumber}
        | LIMIT {limit}
        |""".stripMargin)
      .on("minBlockNumber" -> minBlockNumber.getOrElse(0L), "limit" -> limit.getOrElse(Long.MaxValue))
      .as(parser.*)
  }

}
