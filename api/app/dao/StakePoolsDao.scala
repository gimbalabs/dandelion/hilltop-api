package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL}
import play.api.db.Database
import play.api.libs.json.JsValue

import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import anorm._
import org.joda.time.DateTime
import play.shaded.oauth.org.apache.commons.codec.binary.Hex
import postgresql._

case class StakePoolData(poolIdBech32: String,
                         poolId: Array[Byte],
                         poolMetadataJson: Option[JsValue],
                         activeStake: Long,
                         saturation: Double,
                         pledge: Long,
                         margin: Double,
                         fixedCost: Long,
                         retiringEpoch: Option[Long])

@Singleton
class StakePoolsDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[StakePoolData](ColumnNaming.SnakeCase)

  def activeStakePools(epochNumber: Int) = database.withConnection { implicit c =>

    val stakePoolQuery =
      """
        | WITH active_stake as (SELECT pool_id, SUM (amount) AS active_stake FROM epoch_stake WHERE epoch_no = {epochNumber} GROUP BY pool_id),
        | pool_metadata as (SELECT pool_id, json FROM pool_offline_data WHERE id IN (SELECT max(id) from pool_offline_data GROUP BY pool_id)),
        | circulation as (select sum (treasury + rewards + utxo + deposits + fees) as circulating from ada_pots where epoch_no = {epochNumber})
        | SELECT pu.hash_id, ph.hash_raw as pool_id, ph.view as pool_id_bech32, acs.active_stake, pu.pledge,
        |        pu.margin, pu.fixed_cost, pr.retiring_epoch, pm.json AS pool_metadata_json, acs.active_stake / (SELECT circulating / 500 FROM circulation) AS saturation
        |   FROM pool_update AS pu
        |   LEFT OUTER JOIN pool_retire pr
        |     ON pr.hash_id = pu.hash_id
        |   INNER JOIN pool_hash ph
        |     ON pu.hash_id = ph.id
        |   INNER JOIN active_stake acs
        |     ON pu.hash_id = acs.pool_id
        |   LEFT OUTER JOIN pool_metadata pm
        |     ON pu.hash_id = pm.pool_id
        |   WHERE pu.registered_tx_id in (select max(registered_tx_id) from pool_update group by hash_id)
        |        AND (pr.retiring_epoch is NULL OR pr.retiring_epoch > {epochNumber});
      """.stripMargin


    SQL(stakePoolQuery)
      .on("epochNumber" -> epochNumber)
      .as(parser.*)
  }

  def retiredStakepool(epochNumber: Int = 0) = database.withConnection { implicit c =>

    val stakePoolQuery =
      """
        | WITH active_stake as (SELECT pool_id, SUM (amount) AS active_stake FROM epoch_stake WHERE epoch_no = {epochNumber} GROUP BY pool_id),
        | pool_metadata as (SELECT pool_id, json FROM pool_offline_data WHERE id IN (SELECT max(id) from pool_offline_data GROUP BY pool_id)),
        | circulation as (select sum (treasury + rewards + utxo + deposits + fees) as circulating from ada_pots where epoch_no = {epochNumber})
        | SELECT pu.hash_id, ph.hash_raw as pool_id, ph.view as pool_id_bech32, acs.active_stake, pu.pledge,
        |        pu.margin, pu.fixed_cost, pr.retiring_epoch, pm.json AS pool_metadata_json, acs.active_stake / (SELECT circulating / 500 FROM circulation) AS saturation
        |   FROM pool_update AS pu
        |   INNER JOIN pool_retire pr
        |     ON pr.hash_id = pu.hash_id
        |   INNER JOIN pool_hash ph
        |     ON pu.hash_id = ph.id
        |   INNER JOIN active_stake acs
        |     ON pu.hash_id = acs.pool_id
        |   LEFT OUTER JOIN pool_metadata pm
        |     ON pu.hash_id = pm.pool_id
        |   WHERE pu.registered_tx_id in (select max(registered_tx_id) from pool_update group by hash_id)
        |        AND (pr.retiring_epoch is NULL OR pr.retiring_epoch > {epochNumber});
      """.stripMargin

    SQL(stakePoolQuery)
      .on("epochNumber" -> epochNumber)
      .as(parser.*)
  }

  def liveStake(stakePoolBech32Id: String) = database.withConnection { implicit c =>
    val query =
      """
        |WITH stake AS
        |  (SELECT d1.addr_id
        |   FROM delegation d1, pool_hash
        |   WHERE pool_hash.id=d1.pool_hash_id
        |     AND pool_hash.hash_raw = {hashRaw}
        |     AND NOT EXISTS
        |       (SELECT TRUE
        |        FROM delegation d2
        |        WHERE d2.addr_id=d1.addr_id
        |          AND d2.tx_id>d1.tx_id)
        |     AND NOT EXISTS
        |       (SELECT TRUE
        |        FROM stake_deregistration
        |        WHERE stake_deregistration.addr_id=d1.addr_id
        |          AND stake_deregistration.tx_id>d1.tx_id))
        |SELECT sum(total)
        |FROM
        |  (SELECT sum(value) total
        |   FROM utxo_view
        |     INNER JOIN stake ON utxo_view.stake_address_id=stake.addr_id
        |   UNION SELECT sum(amount)
        |   FROM reward
        |     INNER JOIN stake ON reward.addr_id=stake.addr_id
        |     WHERE reward.spendable_epoch <= (SELECT MAX(epoch_no) FROM block)
        |   UNION SELECT -sum(amount)
        |   FROM withdrawal
        |     INNER JOIN stake ON withdrawal.addr_id=stake.addr_id
        |  ) AS t
      """.stripMargin
    SQL(query)
      .on("hashRaw" -> Hex.decodeHex(stakePoolBech32Id.toCharArray))
      .as(SqlParser.scalar[Long].singleOpt)

  }

  def mintedBlocksInEpoch(stakePoolBech32Id: String, epochNumber: Int) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT count(1) FROM slot_leader sl
        | INNER JOIN block b
        |   ON sl.id = b.slot_leader_id
        | WHERE sl.hash = {hashRaw} and epoch_no = {epochNumber};
        |""".stripMargin)
      .on("epochNumber" -> epochNumber)
      .on("hashRaw" -> Hex.decodeHex(stakePoolBech32Id.toCharArray))
      .as(SqlParser.scalar[Int].singleOpt)
  }

}
