package dao

import anorm.Macro.ColumnNaming
import anorm._
import play.api.db.Database
import play.shaded.oauth.org.apache.commons.codec.binary.Hex

import javax.inject.{Inject, Singleton}

case class PoolMetadataRef(hashRaw: Array[Byte], url: String)

@Singleton
class PoolMetadataRefDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[PoolMetadataRef](ColumnNaming.SnakeCase)

  def findByPoolBechId(poolBech32Id: String) = database.withConnection { implicit c =>
    SQL(
      """
        | SELECT ph.hash_raw, pmr.url
        | FROM pool_metadata_ref pmr
        | INNER JOIN pool_hash ph
        |  ON pmr.pool_id = ph.id
        | WHERE ph.hash_raw = {poolBech32Id}
        | ORDER BY pmr.registered_tx_id DESC
        | LIMIT 1
        |""".stripMargin)
      .on("poolBech32Id" -> Hex.decodeHex(poolBech32Id.toCharArray))
      .as(parser.singleOpt)
  }


}
