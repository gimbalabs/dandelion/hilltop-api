package dao

import anorm.Macro.ColumnNaming
import anorm.{Macro, SQL}
import com.google.inject.{Inject, Singleton}
import db.model.Epoch
import play.api.db.Database

@Singleton
class EpochDao @Inject()(database: Database) {

  private val parser = Macro.namedParser[Epoch](ColumnNaming.SnakeCase)

  def currentEpoch = {
    database.withConnection { implicit c =>
      SQL("SELECT * FROM epoch ORDER BY no DESC LIMIT 1")
        .as(parser.single)
    }
  }

  def findByEpochNumber(epochNumber: Int) = database.withConnection { implicit c =>
    SQL("SELECT * FROM epoch WHERE no = {epochNumber}")
      .on("epochNumber" -> epochNumber)
      .as(parser.singleOpt)
  }

}
